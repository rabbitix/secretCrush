<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crush extends Model
{
    

    public function lovers(){
        return $this->belongsToMany(\App\User::class);
        
    }

    public function telegram_info()
    {
        return $this->hasOne(\App\TelegramInfo::class);
    }

    public function all_info()      
    {
        return $this->hasOne(\App\ExtaInfo::class);
    }
    
}
