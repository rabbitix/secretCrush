<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtaInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exta_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('crush_id')->nullable();
            
            $table->string('height')->nullable();
            $table->string('wight')->nullable();

            $table->string('nick_name')->nullable();

            
            $table->timestamp('birthday_date')->nullable();

            $table->string('where_u_sleep')->nullable();
            $table->string('phone')->nullable();
            $table->string('best_friend_name')->nullable();
            
            // $table->string('')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exta_infos');
    }
}
