<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrushesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crushes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('telegram_info_id')->nullable();
            $table->integer('exta_info_id')->nullable();

            $table->string('name')->nullable();
            $table->string('insta_username')->nullable();
            $table->string('email')->nullable();
            $table->string('city')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crushes');
    }
}
