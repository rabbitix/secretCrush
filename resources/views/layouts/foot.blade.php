<!--jQuery -->
{{--<script type="text/javascript" src="{{asset('plugins/jquery/jquery.min.js')}}"></script>--}}
{{--<script src="https://demos.jquerymobile.com/1.4.2/js/jquery.mobile-1.4.2.min.js"></script>--}}
{{--<script src="{{asset('plugins/font-awesome/js/all.min.js')}}"></script>--}}
<!--jQuery UI 1.11.4-->
<script type="text/javascript" src="{{asset('dist/js/jquery-ui.min.js')}}"></script>
<!--Resolve conflict in jQuery UI tooltip with Bootstrap tooltip-->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!--Bootstrap 4-->
<script type="text/javascript" src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!--Morris.js charts-->
{{--<script type="text/javascript"--}}
{{--src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>--}}
<script type="text/javascript" src="{{asset('plugins/morris/morris.min.js')}}"></script>
<!--Sparkline -->
<script type="text/javascript" src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!--jvectormap -->
<script type="text/javascript" src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!--jQuery Knob Chart-->
<script type="text/javascript" src="{{asset('plugins/knob/jquery.knob.js')}}"></script>
<!--daterangepicker -->
{{--<script type="text/javascript"--}}
{{--src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>--}}
<script type="text/javascript" src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!--datepicker -->
<script type="text/javascript" src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!--Bootstrap WYSIHTML5-->
<script type="text/javascript"
        src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!--Slimscroll -->
<script type="text/javascript" src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!--FastClick -->
<script type="text/javascript" src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!--AdminLTE App-->

<script type="text/javascript" src="{{asset('dist/js/adminlte.min.js')}}"></script>

<!--AdminLTE dashboard demo(This is only for demo purposes) -->
<script type="text/javascript" src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!--AdminLTE for demo purposes-->
<script type="text/javascript" src="{{asset('dist/js/demo.js')}}"></script>
{{--<script src="{{asset('plugins/ionslider/ion.rangeSlider.min.js')}}"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>--}}
<!-- Bootstrap slider -->

<script src="{{asset('dist/js/nouislider.js')}}"></script>
{{--<script src="{{asset('dist/js/rangeslider.js')}}"></script>--}}
<script src="{{asset('plugins/bootstrap-slider/bootstrap-slider.js')}}"></script>
<script src="{{asset('dist/src/kamadatepicker.min.js')}}"></script>

<script src="{{asset('dist/js/alert.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap-multiselect.js')}}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>--}}
{{--<script src="{{asset('plugins/select2/select2.full.js')}}"></script>--}}


<script src="{{asset('dist/js/custom.js')}}"></script>
