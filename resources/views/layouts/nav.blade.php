<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!--Left navbar links-->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="" class="nav-link"> خانه</a>
        </li>
        <form class="form-inline ml-3" action="" method="post">
            {{csrf_field()}}
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" name="input" type="search"
                       placeholder=" " aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

    </ul>


    <!--Right navbar links-->
    <ul class="navbar-nav mr-auto">

            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>
                    <span class="badge badge-danger navbar-badge">
                </span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                    <div style="display: none;">
                    </div>
                            <a href="#" class="dropdown-item">
                                <!-- Message Start -->
                                <div class="media">
                                    <img src="" alt="Avatar"
                                         class="img-size-50 ml-3 img-circle">
                                    <div class="media-body">
                                        <h3 class="dropdown-item-title">
                                            <span class="float-left text-sm text-success"><i
                                                        class="fa fa-star"></i></span>
                                        </h3>
                                        <p class="text-sm"></p>
                                        <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i>
                                        </p>
                                    </div>
                                </div>
                                <!-- Message End -->
                            </a>
                            <div class="dropdown-divider"></div>

                            <div class="hidden" style="display: none;">
                            </div>
                    <a href="" class="dropdown-item dropdown-footer">مشاهده همه
                        پیام‌ها</a>
                </div>
            </li>

    <!-- Notifications Dropdown Menu -->
        {{--<li class="nav-item dropdown">--}}
        {{--<a class="nav-link" data-toggle="dropdown" href="#">--}}
        {{--<i class="fa fa-bell-o"></i>--}}
        {{--<span class="badge badge-warning navbar-badge">15</span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">--}}
        {{--<span class="dropdown-item dropdown-header">15 نوتیفیکیشن</span>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a href="#" class="dropdown-item">--}}
        {{--<i class="fa fa-envelope ml-2"></i> 4 پیام جدید--}}
        {{--<span class="float-left text-muted text-sm">3 دقیقه</span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a href="#" class="dropdown-item">--}}
        {{--<i class="fa fa-users ml-2"></i> 8 درخواست دوستی--}}
        {{--<span class="float-left text-muted text-sm">12 ساعت</span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a href="#" class="dropdown-item">--}}
        {{--<i class="fa fa-file ml-2"></i> 3 گزارش جدید--}}
        {{--<span class="float-left text-muted text-sm">2 روز</span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a href="#" class="dropdown-item dropdown-footer">مشاهده همه نوتیفیکیشن</a>--}}
        {{--</div>--}}
        {{--</li>--}}


        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                        class="fa fa-th-large"></i></a>
        </li>
    </ul>
</nav>