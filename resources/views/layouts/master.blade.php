<!doctype html>
<html lang="en">
<head>
    @include('layouts.head')
    @yield('head')
</head>
{{--<body>--}}
@yield('content')

@include('layouts.foot')
@yield('footer')
{{--</body>--}}
</html>