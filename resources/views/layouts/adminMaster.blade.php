@extends('layouts.master')
@section('head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
@endsection

@section('content')
    <body class="hold-transition sidebar-mini" >
    <div class="wrapper" >

        <!--Navbar -->
    @include('layouts.nav')
    <!-- /.navbar-->

        <!--Main Sidebar Container-->
        @include('layouts.sidebar')

        <!--Content Wrapper . Contains page content-->
        <div class="content-wrapper" style="background-color: #1f2d39" >
            <!--Content Header(Page header) -->
            {{-- <div class="content-header"> --}}
                    {{-- @yield('adminContent') --}}
                     
            {{-- </div> --}}
            <!-- /.content - header-->

            <!--Main content-->
            <section class="content">
                {{-- @include('flash::message') --}}
                    @yield('adminContent')


            </section>
            <!-- /.content-->
        </div>
        <!-- /.content - wrapper-->


        <!--Control Sidebar-->
        <aside class="control-sidebar control-sidebar-dark">
            <!--Control sidebar content goes here-->
        </aside>
        <!-- /.control - sidebar-->
    </div>
    <!-- ./wrapper-->
    @endsection

    @section('footer')
        <script>
            $('input[type="checkbox"]').change(function () {
                if (this.checked) {
                    this.value = 1;
                }
                else {
                    this.value = 0;
                }
            });
        </script>
    </body>
@endsection