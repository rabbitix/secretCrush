<!--Tell the browser to be responsive to screen width-->
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144468040-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-144468040-1');
</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Font Awesome-->
<link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
<!--Ionicons -->
{{--<link rel="stylesheet"--}}
      {{--href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css"/>--}}
{{--<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">--}}
<!--Theme style-->

<link rel="stylesheet" href="{{asset('dist/style/kamadatepicker.min.css')}}">

<link rel="stylesheet" href="{{asset('plugins/ionslider/ion.rangeSlider.css')}}">
<!-- ion slider Nice -->
<link rel="stylesheet" href="{{asset('plugins/ionslider/ion.rangeSlider.skinNice.css')}}">
<!-- bootstrap slider -->
<link rel="stylesheet" href="{{asset('plugins/bootstrap-slider/slider.css')}}">

<!--iCheck -->
<link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
<!--Morris chart-->
<link rel="stylesheet" href="{{asset('plugins/morris/morris.css')}}">
<!--jvectormap -->
<link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
<!--Date Picker-->
<link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
<!--Daterange picker-->
<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">
<!--bootstrap wysihtml5 - text editor-->
<link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
<!--Google Font: Source Sans Pro-->
<link href="{{asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet')}}">
<!--bootstrap rtl-->
<!--template rtl version-->

<link rel="stylesheet" href="{{asset('dist/css/nouislider.css')}}">
{{--<link rel="stylesheet" href="{{asset('dist/css/rangeslider.css')}}">--}}
<link href="https://cdn.rawgit.com/rastikerdar/vazir-font/v20.1.0/dist/font-face.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/custom-style.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/bootstrap-multiselect.css')}}">
<script type="text/javascript" src="{{asset('plugins/jquery/jquery.min.js')}}"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

