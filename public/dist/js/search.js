// $(document).ready(function () {
//     $('#searchHasElevator').removeAttr('checked');
// });
// $(document).change(function () {
// alert($('#searchHasElevator').val());
// });


let propertySearchRoute = $('#propertySearchRoute').val();

$('#searchDealType').change(function () {
    let deal = $(this).val();
    $.post(propertySearchRoute, {
            '_token': $('#token').val()
            , 'dealType': deal
        }
    ).done(function (data) {
        $('.searchRes').html(data);
    });

});

$('#totalPriceMax, #totalPriceMin').change(function () {
    let min = $('#totalPriceMin').val();
    let max = $('#totalPriceMax').val();

    // alert(min + max);
    $.post(propertySearchRoute,
        {
            '_token': $('#token').val(),
            'dealType': deal,
            'totalPriceMin': min,
            'totalPriceMax': max
        }).done(function (data) {
        $('.searchRes').html(data);
    });
});


$('#mortgagePriceMax, #mortgagePriceMin').change(function () {
    let min = $('#mortgagePriceMin').val();
    let max = $('#mortgagePriceMax').val();

    // alert(min + max);
    $.post(propertySearchRoute,
        {
            '_token': $('#token').val(),
            'dealType': deal,
            'mortgagePriceMin': min,
            'mortgagePriceMax': max
        }).done(function (data) {
        $('.searchRes').html(data);
    });
});

$('#rentPriceMin, #rentPriceMax').change(function () {
    let min = $('#mortgagePriceMin').val();
    let max = $('#mortgagePriceMax').val();
    let rmin = $('#rentPriceMin').val();
    let rmax = $('#rentPriceMax').val();

    // alert(min + max);
    $.post(propertySearchRoute,
        {
            '_token': $('#token').val(),
            'dealType': deal,
            'mortgagePriceMin': min,
            'mortgagePriceMax': max,
            'rentPriceMin': rmin,
            'rentPriceMax': rmax
        }).done(function (data) {
        $('.searchRes').html(data);
    });
});

$('#searchDealType,#totalPriceMin,#searchParkingType,#searchOption,#totalPriceMax,#mortgagePriceMin,#mortgagePriceMax,#rentPriceMin,#rentPriceMax,#searchTotalAreaMin,#searchTotalAreaMax,#searchSelectBlv,#searchPropFloorNumber,#buildingAgeMin,#buildingAgeMax,#searchPropSleepRoomCount,#searchKitchenCabinetsType,#searchFloorType,#searchViewType,#searchPropDirections,#searchDocumentType,#searchHasElevator,#searchHasYard,#searchIsOwnerThere').change(function () {
    // alert('ads');
    // if (deal)
    let deal = $('#searchDealType').val();
    let tmin = $('#totalPriceMin').val();
    let tmax = $('#totalPriceMax').val();
    let mmin = $('#mortgagePriceMin').val();
    let mmax = $('#mortgagePriceMax').val();
    let rmin = $('#rentPriceMin').val();
    let rmax = $('#rentPriceMax').val();
    let searchTotalAreaMin = $('#searchTotalAreaMin').val();
    let searchTotalAreaMax = $('#searchTotalAreaMax').val();
    let searchSelectBlv = $('#searchSelectBlv').val();
    let searchPropFloorNumber = $('#searchPropFloorNumber').val();
    let buildingAgeMin = $('#buildingAgeMin').val();
    let buildingAgeMax = $('#buildingAgeMax').val();
    let searchPropSleepRoomCount = $('#searchPropSleepRoomCount').val();
    // let searchKitchenCabinetsType = $('#searchKitchenCabinetsType').val();
    // let searchFloorType = $('#searchFloorType').val();
    // let  searchViewType= $('#searchViewType').val();
    let searchPropDirections = $('#searchPropDirections').val();
    let searchDocumentType = $('#searchDocumentType').val();
    let searchParkingType = $('#searchParkingType').val();
    let searchOptions = $('#searchOption').val();

// alert(searchTotalAreaMax);
    // alert(min + max);
    $.post(propertySearchRoute,
        {
            '_token': $('#token').val(),
            'dealType': deal,
            'totalPriceMin': tmin,
            'totalPriceMax': tmax,
            'mortgagePriceMin': mmin,
            'mortgagePriceMax': mmax,
            'rentPriceMin': rmin,
            'rentPriceMax': rmax,
            'totalAreaMin': searchTotalAreaMin,
            'totalAreaMax': searchTotalAreaMax,
            'BlvMultiselect': searchSelectBlv,
            'propFloorNumber': searchPropFloorNumber,
            'buildingAgeMin': buildingAgeMin,
            'buildingAgeMax': buildingAgeMax,
            'propSleepRoomCount': searchPropSleepRoomCount,
            // 'kitchenCabinetsType':searchKitchenCabinetsType ,
            //
            // 'floorType' : searchFloorType,
            // 'viewType' : searchViewType,
            'propDirections': searchPropDirections,
            'documentType': searchDocumentType,
            'options': searchOptions,
            'parkingType': searchParkingType,


        }).done(function (data) {
        $('.searchRes').html(data);
    });
});


let customerSearchRoute = $('#customerSearchRoute').val();
$(document).ready(function () {
    $.post(customerSearchRoute, {
            '_token': $('#token').val(),
            'all': true
        }).done(function (data) {
            $('#searchResult').html(data);
        });

    }
);

$('#customerName,#customerPhone,#customerDealType,#marriageStatus').change(function () {

    $('#searchResult').html(null);

    let customerName = $('#customerName').val();
    let customerPhone = $('#customerPhone').val();
    let customerDealType = $('#customerDealType').val();
    let marriageStatus = $('#marriageStatus').val();
    $.post(customerSearchRoute, {
        '_token': $('#token').val(),
        'customerName': customerName,
        'customerPhone': customerPhone,
        'customerDealType': customerDealType,
        'marriageStatus': marriageStatus,

    }).done(function (data) {
        $('#searchResult').html(data);
    });

});