"use strict";
var delimiter = " و ", zero = "صفر",
    letters = [["", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"], ["ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده", "بیست"], ["", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"], ["", "یکصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"], ["", " هزار", " میلیون", " میلیارد", " بیلیون", " بیلیارد", " تریلیون", " تریلیارد", "کوآدریلیون", " کادریلیارد", " کوینتیلیون", " کوانتینیارد", " سکستیلیون", " سکستیلیارد", " سپتیلیون", "سپتیلیارد", " اکتیلیون", " اکتیلیارد", " نانیلیون", " نانیلیارد", " دسیلیون", " دسیلیارد"]],
    decimalSuffixes = ["", "دهم", "صدم", "هزارم", "ده‌هزارم", "صد‌هزارم", "میلیونوم", "ده‌میلیونوم", "صدمیلیونوم", "میلیاردم", "ده‌میلیاردم", "صد‌‌میلیاردم"],
    prepareNumber = function (e) {
        var t = e;
        "number" == typeof t && (t = t.toString());
        var r = t.length % 3;
        return 1 === r ? t = "00".concat(t) : 2 === r && (t = "0".concat(t)), t.replace(/\d{3}(?=\d)/g, "$&*").split("*")
    }, threeNumbersToLetter = function (e) {
        if (0 === parseInt(e, 0)) return "";
        var t = parseInt(e, 0);
        if (t < 10) return letters[0][t];
        if (t <= 20) return letters[1][t - 10];
        if (t < 100) {
            var r = t % 10, n = (t - r) / 10;
            return r > 0 ? letters[2][n] + delimiter + letters[0][r] : letters[2][n]
        }
        var i = t % 10, s = (t - t % 100) / 100, u = (t - (100 * s + i)) / 10, a = [letters[3][s]], l = 10 * u + i;
        return l > 0 && (l < 10 ? a.push(letters[0][l]) : l <= 20 ? a.push(letters[1][l - 10]) : (a.push(letters[2][u]), i > 0 && a.push(letters[0][i]))), a.join(delimiter)
    }, convertDecimalPart = function (e) {
        return "" === (e = e.replace(/0*$/, "")) ? "" : (e.length > 11 && (e = e.substr(0, 11)), " ممیز " + Num2persian(e) + " " + decimalSuffixes[e.length])
    }, Num2persian = function (e) {
        if (e = e.toString().replace(/[^0-9.]/g, ""), isNaN(parseFloat(e))) return zero;
        var t = "", r = e, n = e.indexOf(".");
        if (n > -1 && (r = e.substring(0, n), t = e.substring(n + 1, e.length)), r.length > 66) return "خارج از محدوده";
        for (var i = prepareNumber(r), s = [], u = i.length, a = 0; a < u; a += 1) {
            var l = letters[4][u - (a + 1)], o = threeNumbersToLetter(i[a]);
            "" !== o && s.push(o + l)
        }
        return t.length > 0 && (t = convertDecimalPart(t)), s.join(delimiter) + t
    };
String.prototype.toPersianLetter = function () {
    return Num2persian(this)
}, Number.prototype.toPersianLetter = function () {
    return Num2persian(parseFloat(this).toString())
};


var
    persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
    Numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    fixNumbers = function (str) {
        if (typeof str === 'string') {
            for (var i = 0; i < 10; i++) {
                str = str.replace(persianNumbers[i], i).replace(Numbers[i], i);
            }
        }
        return str;
    };


$('.EnNum').keyup(function () {
    $(this).val(fixNumbers($(this).val()));
});

$(document).ready(function () {
    //select2 library
    $('.select2-selection').select2();
});

$('#propTotalPrice').keyup(function () {
    $("#totalPriceSpan").show();
    $("#totalPriceValue").text($(this).val().toPersianLetter());
});

$('#propEachMeterPrice').keyup(function () {
    $("#propEachMeterPriceSpan").show();
    $("#propEachMeterPriceValue").text($(this).val().toPersianLetter());
});

$('#propMortgagePrice').keyup(function () {
    $("#propMortgagePriceSpan").show();
    $("#propMortgagePriceValue").text($(this).val().toPersianLetter());
});

$('#propRentPrice').keyup(function () {
    $("#propRentPriceSpan").show();
    $("#propRentPriceValue").text($(this).val().toPersianLetter());
});


$('#codeGen').ready(function () {
    kamaDatepicker('codeGen', {

        // placeholder text
        placeholder: "تاریخ ثبت آگهی رو انتخاب کن",

        // enable 2 digits
        twodigit: true,

        // close calendar after select
        closeAfterSelect: true,

        // nexy / prev buttons
        nextButtonIcon: "next",
        previousButtonIcon: "previous ",

        // color of buttons
        buttonsColor: 'red',

        // force Farsi digits
        forceFarsiDigits: true,

        // highlight today
        markToday: true,

        // highlight holidays
        markHolidays: true,

        // highlight user selected day
        highlightSelectedDay: true,

        // true or false
        sync: false,

        // display goto today button
        gotoToday: true,

        // the number of years to be selected before this year
        pastYearsCount: 5,

        // the number of years to be selected after this year
        futureYearsCount: 20,

        // auto swaps next/prev buttons
        swapNextPrev: true

    });

});
/*
code part
 */

$('#codeGen').change(function () {
    $('#code').val(function () {
        let text = $('#codeGen').val();
        let rawDate = text.split('/');
        let one = rawDate[0].substr(3);
        let two =  rawDate[1];
        let three = rawDate[2];
        return one+two+three;
    });
});

$('#code').keyup(function () {

    $.post('/admin/property/check_code',
        {
            _token:$('#_token').val(),
            code:$('#code').val()}
            ).done(function (data) {

                if (data==='exists')
                {
                    $('#codeAvailable').hide(500);
                    $('#codeUsed').show(500);

                }
                if(data=== "Dont")
                {
                    $('#codeUsed').hide(500);
                    $('#codeAvailable').show(500);


                }
    });
});



$('#evacuationDate').ready(function () {
    kamaDatepicker('evacuationDate', {

        // placeholder text
        placeholder: "تاریخ تخلیه رو انتخاب کن",

        // enable 2 digits
        twodigit: true,

        // close calendar after select
        closeAfterSelect: true,

        // nexy / prev buttons
        nextButtonIcon: "next",
        previousButtonIcon: "previous ",

        // color of buttons
        buttonsColor: 'red',

        // force Farsi digits
        forceFarsiDigits: true,

        // highlight today
        markToday: true,

        // highlight holidays
        markHolidays: true,

        // highlight user selected day
        highlightSelectedDay: true,

        // true or false
        sync: false,

        // display goto today button
        gotoToday: true,

        // the number of years to be selected before this year
        pastYearsCount: 5,

        // the number of years to be selected after this year
        futureYearsCount: 20,

        // auto swaps next/prev buttons
        swapNextPrev: true

    });

});
$('#rentedStartDate').ready(function () {
    kamaDatepicker('rentedStartDate', {

        // placeholder text
        placeholder: "تاریخ اجاره رو انتخاب کن",

        // enable 2 digits
        twodigit: true,

        // close calendar after select
        closeAfterSelect: true,

        // nexy / prev buttons
        nextButtonIcon: "next",
        previousButtonIcon: "previous",

        // color of buttons
        buttonsColor: 'red',

        // force Farsi digits
        forceFarsiDigits: true,

        // highlight today
        markToday: true,

        // highlight holidays
        markHolidays: true,

        // highlight user selected day
        highlightSelectedDay: true,

        // true or false
        sync: false,

        // display goto today button
        gotoToday: true,

        // the number of years to be selected before this year
        pastYearsCount: 5,

        // the number of years to be selected after this year
        futureYearsCount: 20,

        // auto swaps next/prev buttons
        swapNextPrev: true

    });

});
$('#rentedUntilDate').ready(function () {
    kamaDatepicker('rentedUntilDate', {

        // placeholder text
        placeholder: "تاریخ پایان اجاره رو انتخاب کن",

        // enable 2 digits
        twodigit: true,

        // close calendar after select
        closeAfterSelect: true,

        // nexy / prev buttons
        nextButtonIcon: "next",
        previousButtonIcon: "previous",

        // color of buttons
        buttonsColor: 'red',

        // force Farsi digits
        forceFarsiDigits: true,

        // highlight today
        markToday: true,

        // highlight holidays
        markHolidays: true,

        // highlight user selected day
        highlightSelectedDay: true,

        // true or false
        sync: false,

        // display goto today button
        gotoToday: true,

        // the number of years to be selected before this year
        pastYearsCount: 5,

        // the number of years to be selected after this year
        futureYearsCount: 20,

        // auto swaps next/prev buttons
        swapNextPrev: true

    });

});


$('#requestDate').ready(function () {
    kamaDatepicker('requestDate', {

        // placeholder text
        placeholder: "تاریخ مراجعه",

        // enable 2 digits
        twodigit: true,

        // close calendar after select
        closeAfterSelect: true,

        // nexy / prev buttons
        nextButtonIcon: "next",
        previousButtonIcon: "previous",

        // color of buttons
        buttonsColor: 'red',

        // force Farsi digits
        forceFarsiDigits: true,

        // highlight today
        markToday: true,

        // highlight holidays
        markHolidays: true,

        // highlight user selected day
        highlightSelectedDay: true,

        // true or false
        sync: false,

        // display goto today button
        gotoToday: true,

        // the number of years to be selected before this year
        pastYearsCount: 5,

        // the number of years to be selected after this year
        futureYearsCount: 20,

        // auto swaps next/prev buttons
        swapNextPrev: true

    });

});
$('#untilDate').ready(function () {
    kamaDatepicker('untilDate', {

        // placeholder text
        placeholder: "تا کی وقت داره",

        // enable 2 digits
        twodigit: true,

        // close calendar after select
        closeAfterSelect: true,

        // nexy / prev buttons
        nextButtonIcon: "next",
        previousButtonIcon: "previous",

        // color of buttons
        buttonsColor: 'red',

        // force Farsi digits
        forceFarsiDigits: true,

        // highlight today
        markToday: true,

        // highlight holidays
        markHolidays: true,

        // highlight user selected day
        highlightSelectedDay: true,

        // true or false
        sync: false,

        // display goto today button
        gotoToday: true,

        // the number of years to be selected before this year
        pastYearsCount: 5,

        // the number of years to be selected after this year
        futureYearsCount: 20,

        // auto swaps next/prev buttons
        swapNextPrev: true

    });

});



//region search
$('#dealType,#searchDealType,#customerDealType').change(function () {

    if ($(this).val() === "رهن‌واجاره") {
        $('.rahn').show(500);
        $('.sell').hide(500);
        $('#mortgagePriceMin,#rentPriceMin').val(0);
        $('#mortgagePriceMax,#rentPriceMax').val(999999999999999);
        $('#totalPriceMin,#totalPriceMax').val(null);

    }
    if ($(this).val() === "فروش" || $(this).val() === "خرید") {
        $('.sell').show(500);
        $('.rahn').hide(500);
        $('#totalPriceMin').val(0);
        $('#totalPriceMax').val(999999999999999);
        $('#mortgagePriceMin,#mortgagePriceMax,#rentPriceMin,#rentPriceMax').val(null);


    }
    if ($(this).val() === "فروش‌ـرهن‌واجاره") {
        $('.sell').show(500);
        $('.rahn').show(500);
        $('#mortgagePriceMin,#rentPriceMin').val(0);
        $('#mortgagePriceMax,#rentPriceMax').val(999999999999999);
        $('#totalPriceMin').val(0);
        $('#totalPriceMax').val(999999999999999);

    }
    if ($(this).val() === "معاوضه") {
        $('.sell').hide(500);
        $('.rahn').hide(500);
    }
    if ($(this).val() === null) {
        $('.rahn').hide(500);
        $('.sell').hide(500);
    }
});

$('#totalPriceMin').keyup(function () {
    $("#totalPriceMinSpan").show();
    $("#totalPriceMinVal").text($(this).val().toPersianLetter());
});
$('#totalPriceMax').keyup(function () {
    $("#totalPriceMaxSpan").show();
    $("#totalPriceMaxVal").text($(this).val().toPersianLetter());
});

$('#mortgagePriceMin').keyup(function () {
    $("#mortgagePriceMinSpan").show();
    $("#mortgagePriceMinVal").text($(this).val().toPersianLetter());
});
$('#mortgagePriceMax').keyup(function () {
    $("#mortgagePriceMaxSpan").show();
    $("#mortgagePriceMaxVal").text($(this).val().toPersianLetter());
});

$('#rentPriceMin').keyup(function () {
    $("#rentPriceMinSpan").show();
    $("#rentPriceMinVal").text($(this).val().toPersianLetter());
});
$('#rentPriceMax').keyup(function () {
    $("#rentPriceMaxSpan").show();
    $("#rentPriceMaxVal").text($(this).val().toPersianLetter());
});

//endregion

$('.More').mouseenter(function () {

    $(this).find('.show-more').show(500);

});


$('.More').mouseleave(function () {

    $('.show-more').hide(200);

});

$(document).ready(function() {
    $('#searchSelectBlv,#customerBlv').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'بلوار(ها) را انتخاب کنید',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });
});

$('#showCustomersBtn').click(function () {

    $('#allCustomers').toggle('slow','linear');
});

/*
search part bitch
 */


$(document).ready(function() {
    $('#searchPropFloorNumber').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'طبقه',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });

    $('#searchPropSleepRoomCount').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'تعداد خواب',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });

    $('#searchKitchenCabinetsType').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'نوع کابینت',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });



    $('#searchFloorType').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'نوع کف',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });

    $('#searchViewType').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'نوع نما',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });

    $('#searchPropDirections').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'جهت ملک',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });

    $('#searchDocumentType').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'نوع سند',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });
    $('#searchParkingType').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'نوع پارکینگ',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });
    $('#searchOption').multiselect({
        buttonWidth : '200px',
        // buttonWidth : 250,
        maxHeight: 350,

        enableFiltering: true,
        nonSelectedText: 'امکانات',
        buttonClass: 'btn btn-dark',
        inheritClass: true,
        numberDisplayed: 2,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'جستجو',
        includeResetOption :true,
        includeResetDivider: true,
        resetText: "لغو"

    });


    $('#ageVal').text($('#searchTotalArea').val());

    var slider = document.getElementById("buildingAgeMax");
    var output = document.getElementById("ageVal");
    output.innerHTML = slider.value;

    slider.oninput = function() {
        output.innerHTML = this.value;

    }


});

